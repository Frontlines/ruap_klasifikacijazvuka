using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using GalaSoft.MvvmLight.Command;
using NAudio.CoreAudioApi;
using NAudio.Wave;

namespace RUAP_KlasifikacijaZvuka.ViewModel
{
    public class MainViewModel : ViewModelBase, IDisposable
    {
        private string _currentFileName;
        private bool _isRecording = false;
        public string ActionText { get; set; } = "Start Recording";

        public IEnumerable<MMDevice> AudioInputDevices { get; set; }
        private MMDevice _selectedInputDevice;
        public MMDevice SelectedInputDevice
        {
            get => _selectedInputDevice;
            set
            {
                if (_selectedInputDevice != value)
                {
                    _selectedInputDevice = value;
                    GetDefaultRecordingFormat(_selectedInputDevice);
                }
            }
        }

        private WasapiCapture _capture;
        private WaveFileWriter _writer;

        public int SampleRate { get; set; } = 8000; // 8kHz
        public int Channels { get; set; } = 1; // mono
        public int BitDepth { get; set; } = 16;
        private int _sampleTypeIndex;
        public int SampleTypeIndex
        {
            get => _sampleTypeIndex;
            set
            {
                if (_sampleTypeIndex != value)
                {
                    _sampleTypeIndex = value;
                    
                    BitDepth = _sampleTypeIndex == 1 ? 16 : 32;
                    IsBitDepthConfigurable = SampleTypeIndex == 1;
                }
            }
        }
        public int ShareModeIndex { get; set; } = 0;
        public bool IsBitDepthConfigurable { get; set; } = false;
        private float _recordLevel = 0.5f;
        public float RecordLevel
        {
            get => _recordLevel;
            set
            {
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if (_recordLevel != value)
                {
                    _recordLevel = value;
                    if (_capture != null)
                    {
                        SelectedInputDevice.AudioEndpointVolume.MasterVolumeLevelScalar = value;
                    }
                }
            }
        }
        public float Peak { get; set; }

        private readonly SynchronizationContext _syncContext;

        public RecordingsViewModel RecordingsViewModel { get; }

        public RelayCommand RefreshAudioInputDevicesCommand => new RelayCommand(GetAudioInputDevices);
        public RelayCommand StartStopRecordingCommand => new RelayCommand(StartStopRecording);

        public MainViewModel()
        {
            _syncContext = SynchronizationContext.Current;
            GetAudioInputDevices();

            RecordingsViewModel = new RecordingsViewModel();
        }

        private void GetAudioInputDevices()
        {
            var enumerator = new MMDeviceEnumerator();
            AudioInputDevices = enumerator.EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.Active).ToArray();
            var defaultDevice = enumerator.GetDefaultAudioEndpoint(DataFlow.Capture, Role.Console);
            SelectedInputDevice = AudioInputDevices.FirstOrDefault(c => c.ID.Contains(defaultDevice.ID));
        }
        
        private void StartStopRecording()
        {
            if (_isRecording)
            {
                _capture?.StopRecording();

                _isRecording = false;
            }
            else
            {
                try
                {
                    _capture = new WasapiCapture(SelectedInputDevice);
                    _capture.ShareMode = ShareModeIndex == 0 ? AudioClientShareMode.Shared : AudioClientShareMode.Exclusive;
                    _capture.WaveFormat = SampleTypeIndex == 0
                        ? WaveFormat.CreateIeeeFloatWaveFormat(SampleRate, Channels)
                        : new WaveFormat(SampleRate, BitDepth, Channels);
                    _currentFileName = String.Format("recording_ {0:yyy-MM-dd HH-mm-ss}.wav", DateTime.Now);
                    _capture.DataAvailable += CaptureOnDataAvailable;
                    _capture.RecordingStopped += CaptureOnRecordingStopped;
                    _capture.StartRecording();

                    _isRecording = true;
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }

            ActionText = _isRecording ? "Stop Recording" : "Start Recording";
        }

        private void CaptureOnDataAvailable(object sender, WaveInEventArgs e)
        {
            if (_writer == null)
            {
                _writer = new WaveFileWriter(Path.Combine(RecordingsViewModel.OpenFolderPath,
                        _currentFileName),
                    _capture.WaveFormat);
            }

            _writer.Write(e.Buffer, 0, e.BytesRecorded);

            UpdatePeakMeter();
        }

        void UpdatePeakMeter()
        {
            // can't access this on a different thread from the one it was created on, so get back to GUI thread
            _syncContext.Post(s => Peak = SelectedInputDevice.AudioMeterInformation
                .MasterPeakValue, null);
        }

        private void CaptureOnRecordingStopped(object sender, StoppedEventArgs e)
        {
            _writer.Dispose();
            _writer = null;
            RecordingsViewModel.Recordings.Add(_currentFileName);
            RecordingsViewModel.SelectedRecording = _currentFileName;
            _capture?.Dispose();
            _capture = null;
        }

        private void GetDefaultRecordingFormat(MMDevice value)
        {
            if (value == null) return;

            using (var c = new WasapiCapture(value))
            {
                SampleTypeIndex = c.WaveFormat.Encoding == WaveFormatEncoding.IeeeFloat ? 0 : 1;
                SampleRate = c.WaveFormat.SampleRate;
                BitDepth = c.WaveFormat.BitsPerSample;
                Channels = c.WaveFormat.Channels;
            }
        }

        public void Dispose()
        {
            _capture?.StopRecording();
        }
    }
}