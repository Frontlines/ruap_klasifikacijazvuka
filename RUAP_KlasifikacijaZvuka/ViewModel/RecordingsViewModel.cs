﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Windows.Forms;
using GalaSoft.MvvmLight.Command;
using Newtonsoft.Json;

namespace RUAP_KlasifikacijaZvuka.ViewModel
{
    public class RecordingsViewModel : ViewModelBase
    {
        public struct ClassificationDisplayValues
        {
            public string AirConditioner { get; set; }
            public string CarHorn { get; set; }
            public string ChildrenPlaying { get; set; }
            public string DogBark { get; set; }
            public string Drilling { get; set; }
            public string EngineIdling { get; set; }
            public string GunShot { get; set; }
            public string Jackhammer { get; set; }
            public string Siren { get; set; }
            public string StreetMusic { get; set; }
        }

        private FolderBrowserDialog _folderBrowserDialog = new FolderBrowserDialog();
        public string OpenFolderPath { get; set; }
        public ObservableCollection<string> Recordings { get; }
        public string SelectedRecording { get; set; }
        public string AudioClassification { get; set; } = "";
        public ObservableCollection<ClassificationDisplayValues> ClassificationValues { get; }

        public RelayCommand PlayCommand => new RelayCommand(Play);
        public RelayCommand OpenFolderCommand => new RelayCommand(OpenFolder);
        public RelayCommand PickFolderCommand => new RelayCommand(PickFolder);
        public RelayCommand ClassifyCommand => new RelayCommand(Classify);

        public RecordingsViewModel()
        {
            ClassificationValues = new ObservableCollection<ClassificationDisplayValues>();
            Recordings = new ObservableCollection<string>();
            OpenFolderPath = Path.Combine(Path.GetTempPath(), "RuapKlasifikacijaZvuka");
            Directory.CreateDirectory(OpenFolderPath);
            GetAudioFilesFromDirectory();
            _folderBrowserDialog.Description = "Select Recordings Folder";
            _folderBrowserDialog.SelectedPath = OpenFolderPath;
            _folderBrowserDialog.ShowNewFolderButton = true;
        }

        private void OpenFolder()
        {
            if (OpenFolderPath != null)
            {
                Process.Start(OpenFolderPath);
            }
        }

        private void PickFolder()
        {
            var result = _folderBrowserDialog.ShowDialog();

            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(_folderBrowserDialog.SelectedPath))
            {
                OpenFolderPath = _folderBrowserDialog.SelectedPath;
                GetAudioFilesFromDirectory();
            }
        }

        private void GetAudioFilesFromDirectory()
        {
            foreach (var file in Directory.GetFiles(OpenFolderPath, "*.wav", SearchOption.TopDirectoryOnly))
            {
                Recordings.Add(file);
            }
        }

        private void Play()
        {
            if (SelectedRecording != null)
            {
                Process.Start(Path.Combine(OpenFolderPath, SelectedRecording));
            }
        }

        private void Classify()
        {
            #region InvokePythonScriptToGetMfccValues
            // invoke python script to process audio file at passed path
            string process = @"./MfccFeatureExtraction.exe";
            //C:\Users\JURAJ.JURAJ-PC\Documents\Visual Studio 2017\Projects\AudioFeatureExtraction\AudioFeatureExtraction\MfccFeatureExtraction.py

            ProcessStartInfo pInfo = new ProcessStartInfo(process, "\"" + Path.Combine(OpenFolderPath, SelectedRecording) + "\"");

            pInfo.UseShellExecute = false;
            pInfo.CreateNoWindow = true;
            pInfo.RedirectStandardOutput = true;
            pInfo.RedirectStandardError = true;

            Process proc = new Process();

            proc.StartInfo = pInfo;

            proc.Start();
            proc.PriorityClass = ProcessPriorityClass.High;

            StreamReader stream = proc.StandardOutput;
            string procOutput = stream.ReadToEnd();

            proc.WaitForExit();
            proc.Close();
            #endregion

            #region ParseOutput
            if (string.IsNullOrEmpty(procOutput) || procOutput.Contains("Path doesn't exist"))
            {
                MessageBox.Show("Something went wrong while processing the file.");
                return;
            }

            var charsToRemove = new string[] { "[", "]", "\r", "\n" };
            foreach (var c in charsToRemove)
            {
                procOutput = procOutput.Replace(c, string.Empty);
            }

            var parsedStringArray = procOutput.Trim('[', ']', '\r', '\n').Split(' ')
                .Where(s => !string.IsNullOrEmpty(s)).ToArray();

            if (parsedStringArray.Length <= 0)
            {
                MessageBox.Show("Something went wrong while parsing output.");
                return;
            }

            parsedStringArray = parsedStringArray.Append("value").ToArray();
            #endregion

            // Invoke classification request with method.
            var table = new StringTable()
            {
                ColumnNames = new string[]
                {
                    "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17",
                    "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33",
                    "34", "35", "36", "37", "38", "39", "Class"
                },
                Values = new string[,]
                {
                    {
                        parsedStringArray[0], parsedStringArray[1], parsedStringArray[2], parsedStringArray[3],
                        parsedStringArray[4], parsedStringArray[5], parsedStringArray[6], parsedStringArray[7],
                        parsedStringArray[8], parsedStringArray[9], parsedStringArray[10], parsedStringArray[11],
                        parsedStringArray[12], parsedStringArray[13], parsedStringArray[14], parsedStringArray[15],
                        parsedStringArray[16], parsedStringArray[17], parsedStringArray[18],
                        parsedStringArray[19],
                        parsedStringArray[20], parsedStringArray[21], parsedStringArray[22], parsedStringArray[23],
                        parsedStringArray[24], parsedStringArray[25], parsedStringArray[26], parsedStringArray[27],
                        parsedStringArray[28], parsedStringArray[29], parsedStringArray[30], parsedStringArray[31],
                        parsedStringArray[32], parsedStringArray[33], parsedStringArray[34], parsedStringArray[35],
                        parsedStringArray[36], parsedStringArray[37], parsedStringArray[38],
                        parsedStringArray[39],
                        parsedStringArray[40]
                    }
                }
            };

            var task = Task.Run(() => InvokeRequestResponseService(table));
            Task.WaitAll(task);

            AudioClassification = task.Result[10];
            ClassificationValues.Clear();
            ClassificationDisplayValues cv = new ClassificationDisplayValues
            {
                AirConditioner = task.Result[0],
                CarHorn = task.Result[1],
                ChildrenPlaying = task.Result[2],
                DogBark = task.Result[3],
                Drilling = task.Result[4],
                EngineIdling = task.Result[5],
                GunShot = task.Result[6],
                Jackhammer = task.Result[7],
                Siren = task.Result[8],
                StreetMusic = task.Result[9]
            };
            ClassificationValues.Add(cv);
        }

        #region ResponseRequestClasses
        private class StringTable
        {
            public string[] ColumnNames { get; set; }
            public string[,] Values { get; set; }
        }
        private class Rootobject
        {
            public Results Results { get; set; }
        }
        private class Results
        {
            public Classification classification { get; set; }
        }
        private class Classification
        {
            public string type { get; set; }
            public Value value { get; set; }
        }
        private class Value
        {
            public string[] ColumnNames { get; set; }
            public string[] ColumnTypes { get; set; }
            public string[][] Values { get; set; }
        }
        #endregion

        private async Task<string[]> InvokeRequestResponseService(StringTable sTable)
        {
            using (var client = new HttpClient())
            {
                var scoreRequest = new
                {
                    Inputs = new Dictionary<string, StringTable>() {
                        {
                            "inputMfcc",
                            sTable
                        },
                    },
                    GlobalParameters = new Dictionary<string, string>()
                    {
                    }
                };

                const string apiKey = "VpcvROlA2BC11cPVNTHyUcZcRnm00m/iD/ilklRn4OO97PAxlJhI3smVpJ8VgtWHmRZ9OZApkd+KUP8LzkxoIA=="; // Replace this with the API key for the web service
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiKey);

                client.BaseAddress = new Uri("https://ussouthcentral.services.azureml.net/workspaces/2236d519f79445eeb14a68287613b58f/services/961ab4f0f1f949919503e31a67f23d70/execute?api-version=2.0&details=true");

                HttpResponseMessage response = await client.PostAsJsonAsync("", scoreRequest);
                string[] retres = new string[10];

                if (response.IsSuccessStatusCode)
                {
                    var result = await response.Content.ReadAsStringAsync();

                    Rootobject data = JsonConvert.DeserializeObject<Rootobject>(result);

                    retres = data.Results.classification.value.Values[0];
                }
                else
                {
                    MessageBox.Show("Error while sending classification request.");
                    //Console.WriteLine(string.Format("The request failed with status code: {0}", response.StatusCode));

                    // Print the headers - they include the requert ID and the timestamp, which are useful for debugging the failure
                    //Console.WriteLine(response.Headers.ToString());

                    //string responseContent = await response.Content.ReadAsStringAsync();
                    //Console.WriteLine(responseContent);
                }

                return retres;
            }
        }
    }
}
