﻿using System.ComponentModel;
using PropertyChanged;

namespace RUAP_KlasifikacijaZvuka.ViewModel
{
    [AddINotifyPropertyChangedInterface]
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };
    }
}
