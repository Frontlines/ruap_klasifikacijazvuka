import librosa as lib
import librosa.display as libd
import soundfile as sf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
import scipy

PATH_TO_CSV=r'G:\_ETFOS\Diplomski\RUAP\Projekt'
PATH_TO_AUDIO_DATA=r'G:\_ETFOS\Diplomski\RUAP\Projekt\Train'

SECONDS = 8
SAMPLERATE = 32000
#FEATURE_METHOD = 'fft' #available feature extraction methods: fft, mfcc and rmse.
SAMPLE_COUNT = 3000

def plot_fft(y):
    N = SECONDS*SAMPLERATE
    Xdb = 20*scipy.log10(scipy.absolute(y))
    f = scipy.linspace(0, SAMPLERATE, N, endpoint=False)

    plt.plot(f, Xdb)
    plt.show()

def plot_mfcc(y):
    #plt.figure(figsize=(10,4))
    libd.specshow(y, x_axis='time')
    plt.colorbar()
    plt.title('MFCC')
    plt.tight_layout()
    plt.show()

def plot_rmse(y, rmse):
    S, phase = lib.magphase(lib.stft(y))

    plt.subplot(2,1,1)
    plt.semilogy(rmse.T, label='RMS Energy')
    plt.xticks([])
    plt.xlim([0, rmse.shape[-1]])
    plt.legend(loc='best')
    plt.subplot(2,1,2)
    libd.specshow(lib.amplitude_to_db(S, ref=np.max), y_axis='log', x_axis='time')
    plt.title('log Power spectrogram')
    plt.tight_layout()
    plt.show()

def extract_features(file_name):
   
    try:
        audio, sample_rate = sf.read(file_name)
        audio, sample_rate = sf.read(file_name, start=0, frames=sample_rate*SECONDS, fill_value=0.0)
        audio = lib.to_mono(np.transpose(audio))
        audio = lib.resample(y=audio, orig_sr=sample_rate, target_sr=22050)
        mfccs = lib.feature.mfcc(y=audio, sr=22050, n_mfcc=40)
        mfccsscaled = np.mean(mfccs.T,axis=0)
        #mfccsnorm = 2.*(mfccsscaled - np.min(mfccsscaled))/np.ptp(mfccsscaled)-1
        
    except Exception as e:
        print("Error encountered while parsing file: ", file_name)
        return None 
     
    return mfccsscaled

audio_df = pd.read_csv(PATH_TO_CSV + '\\train.csv', delimiter=',', decimal='.', index_col=0)
#audio_df = audio_df.assign(mfcc=pd.Series(index=audio_df.index, dtype=object))

audio_df = audio_df.head(SAMPLE_COUNT)

count = 0
features = []

for index, row in audio_df.iterrows():
    # y - audio time series values | sr - sampling rate of loaded audio
    try:
        file_name = PATH_TO_AUDIO_DATA + '\\' + str(index) + '.wav'
        class_label = row["Class"]
        data = extract_features(file_name)
        features.append([data, class_label])
        #y, sr = sf.read(PATH_TO_AUDIO_DATA + '\\' + str(file) + '.wav')
        #y, sr = sf.read(PATH_TO_AUDIO_DATA + '\\' + str(file) + '.wav', start=0, frames=sr*SECONDS, fill_value=0.0)
        #y = lib.to_mono(np.transpose(y))
        #y = lib.resample(y, sr, SAMPLERATE)
    except:
        print(index, 'failed to load.')
        continue
    
    #plt.clf()
    #fft = scipy.absolute(scipy.fft(y))
    #audio_df.at[file, 'fft'] = np.mean(fft, axis=0)
    ##plot_fft(fft)
    #audio_df.at[file, 'mfcc'] = np.mean(lib.feature.mfcc(y, sr=SAMPLERATE, n_mfcc=40).T)
    ##plot_mfcc(mfcc)
    #stft = np.abs(lib.stft(y))
    #audio_df.at[file, 'stft'] = np.mean(stft)
    #audio_df.at[file, 'chroma'] = np.mean(lib.feature.chroma_stft(S=stft, sr=SAMPLERATE).T)
    #audio_df.at[file, 'mel'] = np.mean(lib.feature.melspectrogram(y, sr=SAMPLERATE).T)

    count += 1
    print(count, '/', len(audio_df.index))

#audio_df.reset_index(level=0, inplace=True, col_fill='ID')
#print(audio_df.head(3))

#audio_df = audio_df.fft.apply(pd.Series).merge(audio_df, right_index = True, left_index = True).drop([FEATURE_METHOD], axis = 1).melt(id_vars = ['ID', 'Class'], value_name = FEATURE_METHOD).drop('variable', axis = 1).dropna() # id_vars = 'ID', 'Class' - train, 'ID' - test
#audio_df.to_csv(PATH_TO_CSV + '\\featured.csv', index=False, sep=',', decimal='.') # iz nekog razloga parametri index i index_label ne rade ako se koristi parametar sep.
#print("done")

# Iterate through each sound file and extract the features 
#for index, row in metadata.iterrows():
    
#    file_name = os.path.join(os.path.abspath(fulldatasetpath),'fold'+str(row["fold"])+'/',str(row["slice_file_name"]))
    
#    class_label = row["class_name"]
#    data = extract_features(file_name)
    
#    features.append([data, class_label])

# Convert into a Panda dataframe 
featuresdf = pd.DataFrame(features, columns=['feature','Class'])
#featuresdf = pd.DataFrame(featuresdf['feature'].values.tolist(), columns=np.arange(SECONDS * SAMPLE_COUNT))
featuresdff = featuresdf['feature'].apply(pd.Series)
featuresdff = featuresdff.join(featuresdf['Class'])
featuresdff.to_csv(PATH_TO_CSV + '\\featured.csv', index=False, sep=',', decimal='.')

print('Finished feature extraction from ', len(featuresdf), ' files')