import librosa as lib
import soundfile as sf
import numpy as np
import os
import sys

SECONDS = 8
SAMPLERATE = 22050

def extract_features(file_name):
   
    try:
        audio, sample_rate = sf.read(file_name)
        audio, sample_rate = sf.read(file_name, start=0, frames=sample_rate*SECONDS, fill_value=0.0)
        audio = lib.to_mono(np.transpose(audio))
        audio = lib.resample(y=audio, orig_sr=sample_rate, target_sr=SAMPLERATE)
        mfccs = lib.feature.mfcc(y=audio, sr=SAMPLERATE, n_mfcc=40)
        mfccsscaled = np.mean(mfccs.T, axis=0)
        
    except Exception as e:
        print("Error encountered while parsing file: ", file_name)
        return None 
     
    return mfccsscaled

fn = sys.argv[1]
if not os.path.exists(fn):
    print ("Path doesn't exist.")
    # file exists

mfcc = extract_features(fn)
np.set_printoptions(precision=6, suppress=True)
print (mfcc)
